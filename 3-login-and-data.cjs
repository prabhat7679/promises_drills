/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)  */
const fs = require('fs')

const file1 = './newfile1.txt';
const file2 = './newfile2.txt';

const promise1= new Promise((resolve, reject)=>{
    fs.writeFile(file1,'',(err)=>{
        if(err)
        {
             reject(err);
        }else
        {
            resolve()
        }
    })
})

const promise2= new Promise((resolve, reject)=>{
    fs.writeFile(file3,'',(err)=>{
        if(err)
        {
             reject(err);
        }else
        {
            console.log('Two files have been created successfully')
            resolve();
        }
    })
})

Promise.all([promise1,promise2])
.catch((err)=>{
    console.log(err);
}).then(()=>{
    Delete(file1,file2)
})


function Delete(file1,file2)
{
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            fs.unlink(file1,(err)=>{
                if(err)
                {
                    console.error(err)
                }
                
                fs.unlink(file2,(err)=>{
                    if(err)
                    {
                        console.error(err)
                    }
                    resolve()
                })
                
            })
           },2000)
    })
}





/*Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/